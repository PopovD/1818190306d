const btn = document.querySelector('.sendMessage__btn');
const textArea = document.querySelector('.sendMessage__text');
const minusHeight = document.querySelector('.commentContainer__info-link')

textArea.onkeydown = function (e) {
    if ((event.ctrlKey) && ((event.keyCode == 0xA) || (event.keyCode == 0xD))) {
        if (textArea.value == 0) {
            textArea.style.borderColor = 'red';
        } else {
            let newMassage = new Message();
            textArea.value = '';
            textArea.style.borderColor = '#000000';
        }
    }
}

btn.addEventListener('click', () => {
    if (textArea.value == 0) {
        textArea.style.borderColor = 'red';
    } else {
        let newMassage = new Message();
        textArea.value = '';
        textArea.style.borderColor = '#000000';
    }
})

class Message {
    constructor(text) {
        this.node = this.createNede();
    }
    createNede() {
        let container = document.querySelector('.messageContainer');
        let message = `
                <div class="messageContainer__message">
                <h3 class="userInfo__name">Лилия Семёновна<span class="userInfo__name-data">14 октября 2011 </span></h3>
                <div class="massageConteiner__message-text">
                <p>${textArea.value} </p>
                </div>
            </div>		
        `
        container.insertAdjacentHTML('beforeEnd', message)
    }
}